FROM node:14.0.0-alpine3.11

WORKDIR /usr/src/app

RUN apk add --update-cache python make g++ sqlite-dev sqlite

COPY ./package.json .
COPY ./package-lock.json .

RUN npm install

ADD . /usr/src/app
RUN npm run build
CMD ["sh", "-c", "npm run prisma_migrate && npm run go"]
EXPOSE 3000
