import { WebAPICallResult } from "@slack/web-api";

export interface MessageEvent {
    type: string /* equals "message" */;
    subtype: string;
    text: string /* message text */;
    user: string /* user id e.g. UP5A... */;
    team: string /* Team ID */;
    channel: string /* Channel ID */;
    event_ts: string /* timestamp event was issues */;
    ts: string /* Timestamp the event acutally happened */;
    bot_id?: string; /*Present if a bot posted this message*/
}

export interface ResponseEvent extends WebAPICallResult {
    channel: string;
    message: MessageEvent;
}

export interface Channel extends WebAPICallResult {
    id: string;
    name: string;
    is_channel: boolean;
    is_group: boolean;
    is_im: boolean;
    created: number;
    is_archived: boolean;
    is_general: boolean;
    num_members: number;
}

export interface ChannelListing extends WebAPICallResult {
    channels: Channel[];
}

export interface User {
    id: string;
    team_id: string;
    name: string;
    deleted: boolean;
    color: string;
    real_name: string;
    is_admin: boolean;
    is_owner: boolean;
    is_primary_owner: boolean;
    is_restricted: boolean;
    is_ultra_restricted: boolean;
    is_bot: boolean;
    is_app_user: boolean;
    updated: number;
}

export interface UserListing extends WebAPICallResult {
    members: User[];
}

export interface WebResult {
    ok: boolean;
    error?: string;
}

export interface ChannelCreateResult extends WebResult {
    channel?: Channel;
}

export type UserId = string;
export type ChannelId = string;
