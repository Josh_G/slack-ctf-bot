import { Channel, ChannelId } from "./rtm_types";

export class SlackChannel {
    private channelInfo: Channel;

    constructor(channelInfo: Channel) {
        this.channelInfo = channelInfo;
    }

    public getName(): string {
        return this.channelInfo.name;
    }

    public isArchived(): boolean {
        return this.channelInfo.is_archived;
    }

    public getId(): string {
        return this.channelInfo.id;
    }

    public getLink(name = ""): string {
        const nameText = name ? name : this.getName();
        return `<#${this.getId()}|${nameText}>`;
    }

    public isSameChannel(id: ChannelId): boolean {
        return id == this.channelInfo.id;
    }
}
