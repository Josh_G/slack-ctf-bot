import { CommandFunctionAsync } from "../../core/command";
import { CTF } from "./CTF";
import { ok, Result, err } from "neverthrow";
import { SlackChannel } from "../../slack_interface/channel";
import { Bot } from "../../core/bot";
import { CTFDB } from "./db/CTFDB";
import { logger } from "../../core/config";
import { Challenge } from "./Challenge";
import {
    invisErr,
    resp,
    visErr,
    react,
    BotReply,
    BotError,
} from "../../core/botTypes";
import { getCtfStatus } from "./ctf_status";
import { SlackAPI } from "../../slack_interface/slack_api";
import { doSolveByChannel, solveChallenge } from "./solve";
import { doAddChallenge } from "./addChallenge";
import { Creds } from "./Creds";

export async function postToGeneralChannel(
    db: CTFDB,
    api: SlackAPI,
    challenge: Challenge,
    message: string
): Promise<Result<void, Error>> {
    const owningCtfOrErr = await CTF.getOwningCtf(db, challenge);

    if (owningCtfOrErr.isErr()) {
        const error = owningCtfOrErr.error;
        logger.error(
            `Failed posting ${message} to general channel of challenge ${challenge.name}`,
            error
        );
        return err(error);
    }
    const owningCtf = owningCtfOrErr.value;
    const generalChannel = await api.getChannelById(owningCtf.channelId);

    return generalChannel.map((channel) => {
        api.postMessageToChannel(message, channel);
    });
}

//TODO: Create a plugin interface
export class CTFCommands {
    dbInterface: CTFDB;

    constructor(dbInterface: CTFDB) {
        this.dbInterface = dbInterface;
    }

    /**
     * Called when this plugin should be initialised and registers callbacks and any other setup
     */
    //TODO: Add some middleware that can add the qualifiers like "in a channel|as an admin" whilst checking those constraints
    //TODO: Arg parsing auto docs
    public init = (bot: Bot): void => {
        bot.registerCommand({
            name: "s",
            callback: this.status,
            description: "Get the current status of all running ctfs",
            usage: "!s",
        });
        bot.alias("s", "status");

        bot.registerCommand({
            name: "addctf",
            callback: this.addctf,
            description: "Add a CTF to track by generating a general channel",
            usage: "!addctf <ctf_name>",
        });
        bot.registerCommand({
            name: "addchallenge",
            callback: this.addChallenge,
            description:
                "Adds a challenge to the running ctf by creating a room",
            usage: "!addchallenge <challenge_name> <challenge_type>",
        });
        bot.registerCommand({
            name: "workon",
            callback: this.workon,
            description:
                "Mark yourself as working on a challenge and get added to it's room",
            usage: "!workon <challenge_name>",
        });
        bot.registerCommand({
            name: "solve",
            callback: this.solve,
            description: "Mark your current challenge as solved",
            usage: "!solve (in a challenge channel)",
        });
        bot.registerCommand({
            name: "unsolve",
            callback: this.unsolve,
            description: "Mark your current challenge as un-solved",
            usage: "!unsolve (in a challenge channel)",
        });
        bot.registerCommand({
            name: "archive",
            callback: this.archive,
            description: "Archive all ctf challenge channels and ends the ctf",
            usage: "!archive (in a ctf channel)",
        });

        bot.registerCommand({
            name: "qsolve",
            callback: this.quickSolve,
            description: "Quickly adds a challenge and marks it solved already",
            usage: "!qsolve <challenge_name> <challenge_type>",
        });

        bot.registerCommand({
            name: "addcreds",
            callback: this.addcreds,
            description: "Add credentials for currently active ctf",
            usage: "!addcreds <username> <password>",
        });

        bot.registerCommand({
            name: "creds",
            callback: this.creds,
            description: "Gets credentials for currently active ctf",
            usage: "!creds",
        });

        bot.alias("creds", "showcreds");
    };

    public addcreds: CommandFunctionAsync = async (
        _api,
        command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        _user,
        args
    ) => {
        if (args.length != 2) {
            return ok(resp(command.usage));
        }

        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;
        const [username, password] = args;

        const newCredsOrErr = await Creds.setCredsForCtf(
            this.dbInterface,
            currentCtf,
            username,
            password
        );
        if (newCredsOrErr.isErr()) {
            return err(visErr(`Couldn't set creds for ctf ${currentCtf.name}`));
        }
        const newCreds = newCredsOrErr.value;

        return ok(
            resp(`Set creds \`${newCreds.username}:${newCreds.password}\``)
        );
    };

    public creds: CommandFunctionAsync = async (
        _api,
        _command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        _user,
        _args
    ) => {
        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;
        const credsOrErr = await Creds.getCredsForCtf(
            this.dbInterface,
            currentCtf
        );

        if (credsOrErr.isErr()) {
            return err(visErr(`No creds found!`));
        }

        const creds = credsOrErr.value;

        return ok(resp(`\`${creds.username}:${creds.password}\``));
    };

    public solve: CommandFunctionAsync = async (
        api,
        _command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        user,
        _args
    ) => {
        return await doSolveByChannel(this.dbInterface, api, channel, user);
    };

    public quickSolve: CommandFunctionAsync = async (
        api,
        command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        user,
        args
    ) => {
        if (args.length != 2) {
            return ok(resp(command.usage));
        }

        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;
        const [challengeName, challengeType] = args.map((x) => x.toLowerCase());

        const challengeOrErr = await doAddChallenge(
            api,
            currentCtf,
            challengeName,
            challengeType
        );

        if (challengeOrErr.isErr()) {
            return err(challengeOrErr.error);
        }

        const challenge = challengeOrErr.value;

        await solveChallenge(this.dbInterface, api, challenge, user);

        return ok(null);
    };

    public unsolve: CommandFunctionAsync = async (
        api,
        _command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        user,
        _args
    ) => {
        const challengeOrErr = await Challenge.getChallengeFromChannel(
            this.dbInterface,
            channel
        );

        if (challengeOrErr.isErr()) {
            return err(visErr("Couldn't find this challenge channel!"));
        }

        const challenge = challengeOrErr.value;

        await challenge.markUnsolved();

        await postToGeneralChannel(
            this.dbInterface,
            api,
            challenge,
            `<!here> ${user.real_name} has reset the solve on challenge ${challenge.name}`
        );

        return ok(resp("Marked challenge as unsolved"));
    };

    public workon: CommandFunctionAsync = async (
        slackAPI,
        command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        user,
        args
    ) => {
        if (args.length != 1) {
            return ok(resp(command.usage));
        }

        const challengeName = args[0].toLowerCase();

        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;
        const targetChallengeOrErr = await Challenge.getChallenge(
            this.dbInterface,
            currentCtf,
            challengeName
        );

        if (targetChallengeOrErr.isErr()) {
            return err(visErr(`Can't find challenge ${challengeName}`));
        }

        const targetChallenge = targetChallengeOrErr.value;

        // Register user in the db and invite to challenge room
        const chalWorker = await targetChallenge.registerAndInviteWorker(
            slackAPI,
            user
        );

        return chalWorker.match(
            (_): Result<BotReply, BotError> => ok(react("heavy_check_mark")),
            (error) =>
                err(
                    invisErr(
                        `Failed to find channel with id ${targetChallenge.channel}`,
                        error
                    )
                )
        );
    };

    public addChallenge: CommandFunctionAsync = async (
        slackAPI,
        command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        user,
        args
    ) => {
        if (args.length != 2) {
            return ok(resp(`Usage: ${command.usage}`));
        }

        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;
        const [challengeName, challengeType] = args.map((x) => x.toLowerCase());

        // Add challenge
        const addChallengeOrErr = await doAddChallenge(
            slackAPI,
            currentCtf,
            challengeName,
            challengeType
        );
        if (addChallengeOrErr.isErr()) {
            return err(visErr(`Error adding challenge ${challengeName}`));
        }
        const targetChallenge = addChallengeOrErr.value;

        // Register user in the db and invite to challenge room
        const chalWorker = await targetChallenge.registerAndInviteWorker(
            slackAPI,
            user
        );

        return chalWorker.match(
            (_): Result<BotReply, BotError> => ok(react("heavy_check_mark")),
            (error) =>
                err(
                    invisErr(
                        `Error registering and inviting user to ${targetChallenge.channel}`,
                        error
                    )
                )
        );
    };

    /**
     * TODO: When we archive lookup team place and post in general
     */
    public archive: CommandFunctionAsync = async (
        slackAPI,
        _command,
        _bot,
        channel: SlackChannel,
        _timestamp,
        _user,
        _args
    ) => {
        const currentCtfOrErr = await CTF.fromGeneralChannel(
            this.dbInterface,
            channel
        );

        if (currentCtfOrErr.isErr()) {
            logger.error(
                `Failed to find ctf in channel ${channel.getName()}`,
                currentCtfOrErr.error
            );
            return err(visErr(`Couldn't find ctf in this channel!`));
        }

        const currentCtf = currentCtfOrErr.value;

        if (currentCtf.isArchived()) {
            return err(visErr("Can't archive a CTF that is already archived!"));
        }

        const challenges = await currentCtf.getChallenges();

        const results: Array<Result<null, null>> = await Promise.all(
            challenges.map(async (challenge) => {
                const channelOrErr = await slackAPI.getChannelById(
                    challenge.channel
                );
                if (channelOrErr.isErr()) {
                    logger.error(
                        `Couldn't find channel with channel id: ${challenge.channel} for ${challenge.name}`,
                        channelOrErr.error
                    );
                    return err(null) as Result<null, null>;
                }
                const channel = channelOrErr.value;

                if (channel.isArchived()) {
                    // Don't archive already archived channels
                    logger.info(`${channel.getName()} is already archived`);
                    return ok(null) as Result<null, null>;
                }

                return (await slackAPI.archiveChannel(channel)).mapErr(
                    (error) => {
                        logger.error(
                            `Failed to delete channel ${channel.getName()}`,
                            error
                        );
                        return null;
                    }
                );
            })
        );

        if (results.every((result) => result.isOk())) {
            logger.info(`Marking ${currentCtf.name} as archived!`);
            const res = await currentCtf.archive();

            return res.match(
                (_) => {
                    return ok(resp("Archived challenges!"));
                },
                (error) => {
                    logger.error(
                        `Failed to archive ctf: ${currentCtf.name}`,
                        error
                    );
                    return err(
                        visErr(`Failed to archive ctf: ${currentCtf.name}`)
                    ) as Result<BotReply, BotError>;
                }
            );
        } else {
            return err(visErr("Failed to archive all channels"));
        }
    };

    public status: CommandFunctionAsync = async (
        api,
        _command,
        _bot,
        _channel: SlackChannel,
        _timestamp,
        _user,
        _args
    ) => {
        let result = "Ongoing CTF Status:\n";

        const activeCtfsOrErr = await CTF.getCtfs(this.dbInterface);

        if (activeCtfsOrErr.isErr()) {
            const error = activeCtfsOrErr.error;
            return err(invisErr("Failed getting active ctfs", error));
        }

        const activeCtfs = activeCtfsOrErr.value;

        if (activeCtfs.length === 0) {
            return ok(resp("No currently active CTFs!"));
        }

        const activeStatuses: Array<Result<string, Error>> = await Promise.all(
            activeCtfs.map(async (ctf) => {
                return await getCtfStatus(ctf, api);
            })
        );

        const statusMessages: string = activeStatuses
            .reduce((results: Array<string>, ctfResults) => {
                if (ctfResults.isOk()) {
                    return results.concat(ctfResults.value);
                } else {
                    return results;
                }
            }, [])
            .join("\n");

        result = result + statusMessages;

        return ok(resp(result));
    };

    public addctf: CommandFunctionAsync = async (
        slackAPI,
        command,
        _bot,
        _channel: SlackChannel,
        _timestamp,
        _user,
        args
    ) => {
        if (args.length !== 1) {
            return ok(resp(`Usage: ${command.usage}`));
        }

        const ctfName = args[0].toLowerCase();
        const newChannelOrErr = await slackAPI.createPublicChannel(ctfName);

        if (newChannelOrErr.isErr()) {
            logger.error(
                `Failed to create channel: ${ctfName}`,
                newChannelOrErr.error
            );
            return err(
                visErr(
                    `Failed to create general channel #${ctfName}! Does it already exist?`
                )
            );
        }

        const newChannel = newChannelOrErr.value;

        const newCtfOrErr: Result<CTF, Error> = await CTF.newCtf(
            this.dbInterface,
            ctfName,
            newChannel
        );

        if (newCtfOrErr.isErr()) {
            return err(invisErr("Error making ctf", newCtfOrErr.error));
        }
        const ctf = newCtfOrErr.value;

        return ok(
            resp(`Created CTF ${ctf.name} in channel ${newChannel.getLink()}`)
        );
    };
}
