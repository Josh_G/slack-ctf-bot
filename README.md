# Slack CTF Bot

Bot to help organize CTF sessions.

## Slack App Setup

1. Create a [new Slack app](https://api.slack.com).
    * Note down the following information:
        - __Signing Secret__ under _Basic Information_.
        - __Bot User OAuth Access Token__ under _OAuth & Permissions_.
    * Grant the following scopes under _OAuth & Permissions_.
        - `channels:history`
        - `channels:join`
        - `channels:manage`
        - `channels:read`
        - `chat:write`
        - `commands`
        - `groups:history`
        - `groups:read`
        - `groups:write`
        - `incoming-webhook`
        - `pins:read`
        - `pins:write`
        - `reactions:read`
        - `reactions:write`
        - `usergroups:read`
        - `usergroups:write`
        - `users:read`
        - `users:write`
2. Clone the repository.
3. Create a `.env` in in the root repository with the following content.
    ```
    SLACK_TOKEN="Bot User OAuth Access Token"
    SIGNING_SECRET="Signing Secret"
    ```
4. Start the bot server with `npm run start`.
    * You may need to issue `npm install` first.
    * The server on port 3000 must be exposed, you can use [ngrok](ngrok.com) for this.
5. Install the app to your Slack workspace under _OAuth & Permissions_.
6. Under _Event Subscriptions_ paste in your server URL (available in the terminal if using ngrok) and subscribe to the following Slack events.
    * `message.channels`
    * `message.groups`
    * `reaction_added`
    * `reaction_removed`
7. In your Slack workspace, add your bot to a channel to receive commands under _Details_ -> _More_ -> _Add this app to a channel..._
8. Check if your app is reachable by issuing `!ping` in its channel.
